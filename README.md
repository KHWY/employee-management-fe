# EmployeeManagement

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.

## Change Backend Server BaseUrl
Before running the server, make sure that your backend server base url is correct. You can check it under environments folder > environments.development.ts

````
export const environment = {
    apiBaseUrl: "http://localhost:8080"
};
````

## Development server

Make sure your backend server is up and running.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.


