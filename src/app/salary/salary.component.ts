import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Employee } from '../models/employee';
import { EmployeeService } from '../services/employee.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.css']
})
export class SalaryComponent implements OnInit{

  constructor(private employeeService : EmployeeService, private dialog : MatDialog){  }

  employees : Employee[] = [];
  minimumSalary = 0;
  maximumSalary = 10000;
  order = "+";
  sortValue = "name";

  orderOption = ["ASC", "DESC"];
  sortOptions = ['name', 'login', 'salary', 'id'];
  selectedOption: string = "";

  pageSize = 0;
  pageIndex = 0;
  length = 10;

  hidePageSize = true;
  showPageSizeOptions = false;
  showFirstLastButtons = true;
  disabled = false;

  pageEvent: PageEvent = new PageEvent;

  shortLink: string = "";
  loading: boolean = false; // Flag variable
  file?: File; // Variable to store file

  handlePageEvent(e: PageEvent) {
    this.pageIndex = e.pageIndex;
    this.getEmployees(this.minimumSalary, this.maximumSalary, e.pageIndex, this.order+this.sortValue);
  }

  onMinSalaryInput(minSalary:string){
    this.minimumSalary = Number(minSalary);
  }

  onMaxSalaryInput(maxSalary:string){
    this.maximumSalary = Number(maxSalary);
  }

  onOrderSelect(order:string){
    this.order = order == "ASC"? "+" : "-";
  }

  onSortSelect(sortValue:string){
    this.sortValue = sortValue;
  }

  onFilterClick(){
    this.paginator.firstPage();
    this.getEmployees(this.minimumSalary, this.maximumSalary, this.pageIndex, this.order+this.sortValue)
  }


  displayedColumns: string[] = ['id', 'login', 'name', 'salary', 'action'];
  dataSource!: MatTableDataSource<Employee>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit(): void {
    this.getEmployees(0, 10000, 0, "+name");
  }

  public getEmployees(minSalary:number, maxSalary:number, offset:number, sort:String) : void {
   this.employeeService.getEmployees(minSalary, maxSalary, offset, sort).subscribe({
    next:  (response:any) => {
      this.employees = response.content;
      this.dataSource = new MatTableDataSource(this.employees);
      this.length = response.totalElements;
      this.pageSize = response.size;
    },
    error: (error: HttpErrorResponse) => {
      alert(error.error.message? error.error.message : error.message);
    }
    });
  }

  // On file Select
  onChange(event:any) {
    this.file = event.target.files[0];
  }

  // OnClick of button Upload
  onUpload() {
    this.loading = !this.loading;
    this.employeeService.upload(this.file).subscribe({
        next:(resp: String) => {
          alert("upload success");
          this.getEmployees(this.minimumSalary, this.maximumSalary, this.pageIndex, this.order+this.sortValue);
        },
        error: (error: HttpErrorResponse) => {
          alert(error.error.message? error.error.message : error.message);
        }
      });
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent);

    dialogRef.afterClosed().subscribe(value => {
      if(value === 'created'){this.getEmployees(this.minimumSalary, this.maximumSalary, this.pageIndex, this.order+this.sortValue);
      }
    });
  }

  editEmployee(row: any){
    this.dialog.open(DialogComponent,{
      data: row
    }).afterClosed()
    .subscribe( value => {
      if(value === 'updated'){
        this.getEmployees(this.minimumSalary, this.maximumSalary, this.pageIndex, this.order+this.sortValue);
      }
    });
  }

  deleteEmployee(id:string){
    this.employeeService.deleteEmployee(id)
      .subscribe({
        next: (resp) => {
          alert("employee deleted");
          this.getEmployees(this.minimumSalary, this.maximumSalary, this.pageIndex, this.order+this.sortValue);
        },
        error: (error: HttpErrorResponse) => {
          alert(error.error.message? error.error.message : error.message);
        }
      })
  }

}
