import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private serverUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public getEmployees(minSalary:number, maxSalary:number, offset:number, sort:String): Observable<Employee[]>{
    sort = sort.replace("+","%2B");
    return this.http.get<any>(`${this.serverUrl}/users?minSalary=${minSalary}&maxSalary=${maxSalary}&offset=${offset}&limit=30&sort=${sort}`);
  }

  public createEmployee(employee:Employee): Observable<Employee>{
    return this.http.post<Employee>(`${this.serverUrl}/users`, employee);
  }

  public updateEmployee(id:String, employee:Employee): Observable<Employee>{
    return this.http.put<Employee>(`${this.serverUrl}/users/${id}`, employee);
  }

  public deleteEmployee(employeeId:string): Observable<boolean>{
    return this.http.delete<boolean>(`${this.serverUrl}/users/${employeeId}`);
  }

  public upload(file:any):Observable<any> {
  
    const formData = new FormData(); 
      
    formData.append("file", file, file.name);
      
    return this.http.post(`${this.serverUrl}/users/upload`, formData)
  }

}
