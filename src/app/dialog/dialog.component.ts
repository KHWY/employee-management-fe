import { Component, Inject, inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit{

  actionBtn : String = 'Create'
  employeeForm !: FormGroup;

  constructor(
      private formBuilder:FormBuilder,
      @Inject(MAT_DIALOG_DATA) public editData : any,
      private employeeService:EmployeeService,
      private dialogRef:MatDialogRef<DialogComponent>
    ){

     }

  ngOnInit(): void {
    this.employeeForm = this.formBuilder.group({
      id : ['', Validators.required],
      login : ['', Validators.required],
      name : ['', Validators.required],
      salary : ['', Validators.required]
    });

    if(this.editData){
      this.actionBtn = "Update";
      this.employeeForm.controls['id'].setValue(this.editData.id);
      this.employeeForm.controls['login'].setValue(this.editData.login);
      this.employeeForm.controls['name'].setValue(this.editData.name);
      this.employeeForm.controls['salary'].setValue(this.editData.salary);
    }
  }

  addEmployee(){
    if(!this.editData){
      if(this.employeeForm.valid){
        this.employeeService.createEmployee(this.employeeForm.value)
        .subscribe({
          next: (resp) => {
            alert("employee added");
            this.employeeForm.reset();
            this.dialogRef.close('created');
          },
          error: (error: HttpErrorResponse) => {
            alert(error.error.message? error.error.message : error.message);
          }
        })
      }
    } else{
      this.employeeService.updateEmployee(this.employeeForm.value.id, this.employeeForm.value)
        .subscribe({
          next: (resp) => {
            alert("employee updated");
            this.employeeForm.reset();
            this.dialogRef.close('updated');
          },
          error: (error: HttpErrorResponse) => {
            alert(error.error.message? error.error.message : error.message);
          }
        })
    }
  }

}
